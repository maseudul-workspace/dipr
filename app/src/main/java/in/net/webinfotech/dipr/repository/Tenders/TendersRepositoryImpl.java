package in.net.webinfotech.dipr.repository.Tenders;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.dipr.domain.model.NewsListWrapper;
import in.net.webinfotech.dipr.domain.model.TendersWrapper;
import in.net.webinfotech.dipr.repository.APIclient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 05-02-2019.
 */

public class TendersRepositoryImpl {
    TendersRepository mRepository;

    public TendersRepositoryImpl() {
        mRepository = APIclient.createService(TendersRepository.class);
    }

    public TendersWrapper getTendersList(int pagNo){
        TendersWrapper tendersWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> tendersList = mRepository.getTendersList(pagNo);

            Response<ResponseBody> response = tendersList.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("ErrorMsg", responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    tendersWrapper = null;
                }else{
                    tendersWrapper = gson.fromJson(responseBody, TendersWrapper.class);

                }
            } else {
                tendersWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg",e.getMessage());
            tendersWrapper = null;
        }
        return tendersWrapper;
    }

}
