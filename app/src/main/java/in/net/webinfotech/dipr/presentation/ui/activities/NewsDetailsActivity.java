package in.net.webinfotech.dipr.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.dipr.R;
import in.net.webinfotech.dipr.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.dipr.domain.model.News;
import in.net.webinfotech.dipr.presentation.presenters.NewsDetailsPresenter;
import in.net.webinfotech.dipr.presentation.presenters.impl.NewsDetailsPresenterImpl;
import in.net.webinfotech.dipr.presentation.ui.util.GlideHelper;
import in.net.webinfotech.dipr.threading.MainThreadImpl;

public class NewsDetailsActivity extends AppCompatActivity implements NewsDetailsPresenter.View{

    int newsId;
    NewsDetailsPresenterImpl mPresenter;
    @BindView(R.id.img_news_details_poster)
    ImageView imgNewsPoster;
    @BindView(R.id.txt_news_details_title)
    TextView txtNewsTitle;
    @BindView(R.id.txt_news_details_description)
    TextView txtNewsDescription;
    @BindView(R.id.news_details_progress_bar)
    RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        ButterKnife.bind(this);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        newsId = getIntent().getIntExtra("newsId", 1);
        initialisePresenter();
        mPresenter.getNewsDetails(newsId);
    }

    public void initialisePresenter(){
        mPresenter = new NewsDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadNewsDetails(News news) {
        getSupportActionBar().setTitle(Html.fromHtml("<font color='black'>" + news.title +"</font>"));
        GlideHelper.setImageView(this, imgNewsPoster, getResources().getString(R.string.base_url) + "/uploads/news_image/" + news.image);
        txtNewsTitle.setText(news.title);
        txtNewsDescription.setText(Html.fromHtml(news.description));
        txtNewsDescription.setMovementMethod(LinkMovementMethod.getInstance());
        txtNewsDescription.setLinksClickable(true);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
