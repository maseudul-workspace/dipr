package in.net.webinfotech.dipr.presentation.presenters.base;

import in.net.webinfotech.dipr.domain.executor.Executor;
import in.net.webinfotech.dipr.domain.executor.MainThread;

/**
 * Created by Raj on 04-02-2019.
 */

public abstract class AbstractPresenter {
    protected Executor mExecutor;
    protected MainThread mMainThread;
    public AbstractPresenter(Executor executor, MainThread mainThread){
        this.mExecutor = executor;
        this.mMainThread = mainThread;
    }
}
