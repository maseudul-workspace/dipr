package in.net.webinfotech.dipr.repository.Tenders;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 05-02-2019.
 */

public interface TendersRepository {
    @POST("api/tender_list.php")
    @FormUrlEncoded
    Call<ResponseBody> getTendersList(@Field("page_no") int page_no);
}
