package in.net.webinfotech.dipr.domain.interactors.impl;

import in.net.webinfotech.dipr.domain.executor.Executor;
import in.net.webinfotech.dipr.domain.executor.MainThread;
import in.net.webinfotech.dipr.domain.interactors.GetNewsDetailsInteractor;
import in.net.webinfotech.dipr.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.dipr.domain.model.News;
import in.net.webinfotech.dipr.domain.model.NewsDetailsWrapper;
import in.net.webinfotech.dipr.repository.News.NewsRepositoryImpl;

/**
 * Created by Raj on 05-02-2019.
 */

public class GetNewsDetailsInteractorImpl extends AbstractInteractor implements GetNewsDetailsInteractor {

    Callback mCallback;
    int newsId;
    NewsRepositoryImpl mRepository;

    public GetNewsDetailsInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        NewsRepositoryImpl repository,
                                        int newsId
                                        ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.newsId = newsId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final News news){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsDetailsSuccess(news);
            }
        });
    }


    @Override
    public void run() {
        final NewsDetailsWrapper newsDetailsWrapper = mRepository.getNewsDetails(newsId);
        if(newsDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!newsDetailsWrapper.status){
            notifyError(newsDetailsWrapper.message);
        }else{
            postMessage(newsDetailsWrapper.news);
        }
    }
}
