package in.net.webinfotech.dipr.presentation.ui.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.dipr.R;
import in.net.webinfotech.dipr.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.dipr.domain.model.News;
import in.net.webinfotech.dipr.presentation.presenters.MainPresenter;
import in.net.webinfotech.dipr.presentation.presenters.impl.MainPresenterImpl;
import in.net.webinfotech.dipr.presentation.routers.MainRouter;
import in.net.webinfotech.dipr.presentation.ui.adapters.NewsListAdapter;
import in.net.webinfotech.dipr.presentation.ui.util.GlideHelper;
import in.net.webinfotech.dipr.threading.MainThreadImpl;

public class MainActivity extends AppCompatActivity implements MainPresenter.View, MainRouter {

    @BindView(R.id.recycler_view_main)
    RecyclerView recyclerView;
    @BindView(R.id.top_news_poster)
    ImageView imgTopPoster;
    @BindView(R.id.txt_top_news_title)
    TextView txtTopNewsTitile;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    MainPresenterImpl mPresenter;
    @BindView(R.id.news_swiperefresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.splash_progress_bar)
    RelativeLayout splashProgressBarLayout;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;
    @BindView(R.id.pagination_progress_layout)
    RelativeLayout paginationProgressLayout;
    @BindView(R.id.txt_top_news_decription)
    TextView txtNewsDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        initialisePresenter();
        mPresenter.getNewsList(pageNo, "refresh");
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNo = 1;
                totalPage = 1;
                mPresenter.getNewsList(pageNo, "refresh");
            }
        });
        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View)nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));

                if (diff == 0) {
                    // your pagination code
                    if(pageNo < totalPage) {
                        pageNo = pageNo + 1;
                        showPaginationProgressLayout();
                        mPresenter.getNewsList(pageNo, "");
                    }

                }
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_tenders:
                        Intent intent = new Intent(getApplicationContext(), TendersActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_about:
                        Intent intent1 = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(intent1);
                        break;
                }
                return false;
            }
        });
    }

    public void initialisePresenter(){
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public void loadNewsRecyclerView(NewsListAdapter adapter, int total) {
        totalPage = total;
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
//                {
//                    isScrolling= true;
//                }
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                currentItems = layoutManager.getChildCount();
//                totalItems  = layoutManager.getItemCount();
//                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
//                if(!recyclerView.canScrollVertically(1))
//                {
//                    Log.e("ErrorMsg","Scrolling finished");
//                    if(pageNo < totalPage) {
//                        isScrolling = false;
//                        pageNo = pageNo + 1;
//                        showPaginationProgressLayout();
//                        mPresenter.getNewsList(pageNo, "");
//                    }
//                }
//            }
//        });
    }

    @Override
    public void loadNewsHeader(final News news) {
        GlideHelper.setImageView(this, imgTopPoster, getResources().getString(R.string.base_url) + "/uploads/news_image/thumb/" + news.image);
        txtTopNewsTitile.setText(news.title);
        imgTopPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNewsDetails(news.news_id);
            }
        });

        txtTopNewsTitile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNewsDetails(news.news_id);
            }
        });
        txtNewsDescription.setText(Html.fromHtml(news.description));
        txtNewsDescription.setMovementMethod(LinkMovementMethod.getInstance());
        txtNewsDescription.setLinksClickable(true);

    }

    @Override
    public void showProgressbar() {

    }

    @Override
    public void hideProgressbar() {
        splashProgressBarLayout.setVisibility(View.GONE);
        bottomNavigationView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPaginationProgressLayout() {
        paginationProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationProgressLayout() {
        paginationProgressLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void goToNewsDetails(int newsId) {
        Intent intent = new Intent(this, NewsDetailsActivity.class);
        intent.putExtra("newsId", newsId);
        startActivity(intent);
    }
}
