package in.net.webinfotech.dipr.presentation.routers;

/**
 * Created by Raj on 05-02-2019.
 */

public interface MainRouter {
    void goToNewsDetails(int newsId);
}
