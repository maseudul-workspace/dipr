package in.net.webinfotech.dipr.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.dipr.R;
import in.net.webinfotech.dipr.domain.model.Tenders;

/**
 * Created by Raj on 05-02-2019.
 */

public class TendersListAdapter extends RecyclerView.Adapter<TendersListAdapter.ViewHolder>{

    Tenders[] tenders;
    Context mContext;
    public interface Callback{
        void onViewClicked(String pdfUrl);
        void onDownloadClicked(String pdfUrl);
    }
    Callback mCallback;

    public TendersListAdapter(Context context, Tenders[] tenders, Callback callback) {
        this.mContext = context;
        this.tenders = tenders;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_tender_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.txtTenderTitle.setText(tenders[position].title);
        holder.txtTenderView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onViewClicked(tenders[position].file);
            }
        });
        holder.txtTenderDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onDownloadClicked(tenders[position].file);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tenders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_recycler_view_tender_title)
        TextView txtTenderTitle;
        @BindView(R.id.txt_recycler_view_tender_download)
        TextView txtTenderDownload;
        @BindView(R.id.txt_recycler_view_tender_view)
        TextView txtTenderView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void addData(Tenders[] newTenders){
        tenders = newTenders;
    }

}
