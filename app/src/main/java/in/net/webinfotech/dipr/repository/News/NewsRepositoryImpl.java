package in.net.webinfotech.dipr.repository.News;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.dipr.domain.model.NewsDetailsWrapper;
import in.net.webinfotech.dipr.domain.model.NewsListWrapper;
import in.net.webinfotech.dipr.repository.APIclient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 04-02-2019.
 */

public class NewsRepositoryImpl {
    NewsRepository mRepository;

    public NewsRepositoryImpl() {
        mRepository = APIclient.createService(NewsRepository.class);
    }

    public NewsListWrapper getNewslist(int pageNo){
        NewsListWrapper newsListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> newsList = mRepository.getNewsList(pageNo);

            Response<ResponseBody> response = newsList.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("ErrorMsg", responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    newsListWrapper = null;
                }else{
                    newsListWrapper = gson.fromJson(responseBody, NewsListWrapper.class);

                }
            } else {
                newsListWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg",e.getMessage());
            newsListWrapper = null;
        }
        return newsListWrapper;
    }

    public NewsDetailsWrapper getNewsDetails(int newsId){
        NewsDetailsWrapper newsDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> newsDetails = mRepository.getNewsDetails(newsId);

            Response<ResponseBody> response = newsDetails.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("ErrorMsg", responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    newsDetailsWrapper = null;
                }else{
                    newsDetailsWrapper = gson.fromJson(responseBody, NewsDetailsWrapper.class);

                }
            } else {
                newsDetailsWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg",e.getMessage());
            newsDetailsWrapper = null;
        }
        return newsDetailsWrapper;
    }

}
