package in.net.webinfotech.dipr.domain.interactors;

import in.net.webinfotech.dipr.domain.model.News;

/**
 * Created by Raj on 04-02-2019.
 */

public interface GetNewsListInteractor {
    interface Callback{
        void onGettingNewsListSuccess(News[] news, int totalPage);
        void onGettingNewsFail(String errorMsg);
    }
}
