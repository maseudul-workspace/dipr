package in.net.webinfotech.dipr.domain.interactors;

import in.net.webinfotech.dipr.domain.model.Tenders;

/**
 * Created by Raj on 05-02-2019.
 */

public interface GetTendersListInteractor {
    interface Callback{
        void onGettingTendersListSuccess(Tenders[] tenders, int totalPages);
        void onGettingTendersListFail(String errorMsg);
    }
}
