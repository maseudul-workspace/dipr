package in.net.webinfotech.dipr.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 25-01-2019.
 */

public class News {

    @SerializedName("news_id")
    @Expose
    public int news_id;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("description")
    @Expose
    public String description;

}
