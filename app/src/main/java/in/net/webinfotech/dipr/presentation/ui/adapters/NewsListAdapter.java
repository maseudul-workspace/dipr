package in.net.webinfotech.dipr.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.dipr.R;
import in.net.webinfotech.dipr.domain.model.News;
import in.net.webinfotech.dipr.presentation.ui.util.GlideHelper;

/**
 * Created by Raj on 25-01-2019.
 */

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {

    public interface Callback{
        void onNewsClicked(int NewsId);
    }

    News[] news;
    Context mContext;
    Callback mCallback;

    public NewsListAdapter(News[] news, Context context, Callback callback) {
        this.news = news;
        mContext = context;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_main_layout, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageView(mContext, holder.imgThumbnail, mContext.getResources().getString(R.string.base_url) + "/uploads/news_image/thumb/" + news[position + 1].image);
        holder.txtTitle.setText(news[position + 1].title);
        holder.imgThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onNewsClicked(news[position + 1].news_id);
            }
        });
        holder.txtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onNewsClicked(news[position + 1].news_id);
            }
        });
       holder.txtDescription.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               mCallback.onNewsClicked(news[position + 1].news_id);
           }
       });
        holder.txtDescription.setText(Html.fromHtml(news[position + 1].description));
        holder.txtDescription.setMovementMethod(LinkMovementMethod.getInstance());
        holder.txtDescription.setLinksClickable(true);
    }

    @Override
    public int getItemCount() {
        return news.length - 1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_news_thumbnail)
        ImageView imgThumbnail;
        @BindView(R.id.txt_recycler_view_title)
        TextView txtTitle;
        @BindView(R.id.txt_recycler_view_description)
        TextView txtDescription;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void addItems(News[] newItems){
        this.news = newItems;
    }

}
