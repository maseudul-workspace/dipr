package in.net.webinfotech.dipr.presentation.presenters.impl;

import android.content.Context;

import in.net.webinfotech.dipr.domain.executor.Executor;
import in.net.webinfotech.dipr.domain.executor.MainThread;
import in.net.webinfotech.dipr.domain.interactors.GetNewsListInteractor;
import in.net.webinfotech.dipr.domain.interactors.impl.GetNewsListInteractorImpl;
import in.net.webinfotech.dipr.domain.model.News;
import in.net.webinfotech.dipr.presentation.presenters.MainPresenter;
import in.net.webinfotech.dipr.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.dipr.presentation.routers.MainRouter;
import in.net.webinfotech.dipr.presentation.ui.adapters.NewsListAdapter;
import in.net.webinfotech.dipr.repository.News.NewsRepositoryImpl;

/**
 * Created by Raj on 04-02-2019.
 */

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    GetNewsListInteractor.Callback,
                                                                    NewsListAdapter.Callback{

    Context mContext;
    GetNewsListInteractorImpl mInteractor;
    MainPresenter.View mView;
    NewsListAdapter adapter;
    News[] newNews;
    MainRouter mRouter;

    public MainPresenterImpl(Executor executor,
                             MainThread mainThread,
                             Context context,
                             MainPresenter.View view,
                             MainRouter router) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
        this.mRouter = router;
    }

    @Override
    public void getNewsList(int pageNo, String type) {
        if(type.equals("refresh")){
            newNews = null;
        }
        mInteractor = new GetNewsListInteractorImpl(mExecutor, mMainThread, this, new NewsRepositoryImpl(), pageNo);
        mInteractor.execute();
    }

    @Override
    public void onGettingNewsListSuccess(News[] news, int totalPage) {
        News[] tempNews;
        tempNews = newNews;
        try {
            int len1 = tempNews.length;
            int len2 = news.length;
            newNews = new News[len1 + len2];
            System.arraycopy(tempNews, 0, newNews, 0, len1);
            System.arraycopy(news, 0, newNews, len1, len2);
            adapter.addItems(newNews);
            adapter.notifyDataSetChanged();
            mView.hidePaginationProgressLayout();
        }catch (NullPointerException e){
            newNews = news;
            adapter = new NewsListAdapter(news, mContext, this);
            mView.loadNewsRecyclerView(adapter, totalPage);
            mView.loadNewsHeader(news[0]);
            mView.hideProgressbar();
        }
        mView.stopRefreshing();
    }

    @Override
    public void onNewsClicked(int NewsId) {
        mRouter.goToNewsDetails(NewsId);
    }

    @Override
    public void onGettingNewsFail(String errorMsg) {
        mView.hidePaginationProgressLayout();
        mView.stopRefreshing();
        mView.hideProgressbar();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
