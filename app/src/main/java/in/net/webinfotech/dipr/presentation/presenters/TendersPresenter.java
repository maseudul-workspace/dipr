package in.net.webinfotech.dipr.presentation.presenters;

import in.net.webinfotech.dipr.domain.model.Tenders;
import in.net.webinfotech.dipr.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.dipr.presentation.ui.adapters.TendersListAdapter;

/**
 * Created by Raj on 05-02-2019.
 */

public interface TendersPresenter extends BasePresenter {
    void getTendersList(int pageNo, String type);
    interface View{
        void loadRecyclerViewTenders(TendersListAdapter adapter, int totalPages);
        void showProgressbar();
        void hideProgressbar();
        void showPaginationProgressLayout();
        void hidePaginationProgressLayout();
        void stopRefreshing();
        void startDownload(String pdfUrl);
    }
}
