package in.net.webinfotech.dipr.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import in.net.webinfotech.dipr.R;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }
}
