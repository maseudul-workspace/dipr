package in.net.webinfotech.dipr.domain.interactors.impl;

import in.net.webinfotech.dipr.domain.executor.Executor;
import in.net.webinfotech.dipr.domain.executor.MainThread;
import in.net.webinfotech.dipr.domain.interactors.GetNewsListInteractor;
import in.net.webinfotech.dipr.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.dipr.domain.model.News;
import in.net.webinfotech.dipr.domain.model.NewsListWrapper;
import in.net.webinfotech.dipr.repository.News.NewsRepositoryImpl;

/**
 * Created by Raj on 04-02-2019.
 */

public class GetNewsListInteractorImpl extends AbstractInteractor implements GetNewsListInteractor {

    int pageNo;
    Callback mCallback;
    NewsRepositoryImpl mRepository;

    public GetNewsListInteractorImpl(Executor threadExecutor,
                                     MainThread mainThread,
                                     Callback callback,
                                     NewsRepositoryImpl repository,
                                     int pageNo) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.pageNo = pageNo;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsFail(errorMsg);
            }
        });
    }

    private void postMessage(final News[] news, final int totalPages){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsListSuccess(news, totalPages);
            }
        });
    }

    @Override
    public void run() {
        final NewsListWrapper newsListWrapper = mRepository.getNewslist(pageNo);
        if(newsListWrapper == null){
            notifyError("Something went wrong");
        }else if(!newsListWrapper.status){
            notifyError(newsListWrapper.message);
        }else{
            postMessage(newsListWrapper.news, newsListWrapper.total_page);
        }
    }
}
