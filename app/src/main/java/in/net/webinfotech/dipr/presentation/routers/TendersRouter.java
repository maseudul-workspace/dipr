package in.net.webinfotech.dipr.presentation.routers;

/**
 * Created by Raj on 07-02-2019.
 */

public interface TendersRouter {
    void goToPdfViewActivity(String pdfUrl);
}
