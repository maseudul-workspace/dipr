package in.net.webinfotech.dipr.domain.interactors;

import in.net.webinfotech.dipr.domain.model.News;

/**
 * Created by Raj on 05-02-2019.
 */

public interface GetNewsDetailsInteractor {
    interface Callback{
        void onGettingNewsDetailsSuccess(News news);
        void onGettingNewsDetailsFail(String errorMsg);
    }
}
