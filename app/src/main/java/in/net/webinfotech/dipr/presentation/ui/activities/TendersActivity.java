package in.net.webinfotech.dipr.presentation.ui.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.dipr.R;
import in.net.webinfotech.dipr.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.dipr.presentation.presenters.TendersPresenter;
import in.net.webinfotech.dipr.presentation.presenters.impl.TendersPresenterImpl;
import in.net.webinfotech.dipr.presentation.routers.TendersRouter;
import in.net.webinfotech.dipr.presentation.ui.adapters.TendersListAdapter;
import in.net.webinfotech.dipr.threading.MainThreadImpl;

public class TendersActivity extends AppCompatActivity implements TendersPresenter.View, TendersRouter {

    TendersPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_tenders)
    RecyclerView recyclerView;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    ProgressDialog progressDialog;
    String[] appPremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    String finalUrl = "";
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    @BindView(R.id.splash_progress_bar)
    RelativeLayout progressBarLayout;
    @BindView(R.id.pagination_progress_layout)
    RelativeLayout paginationProgressLayout;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenders);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow_back);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);
        progressDialog = new ProgressDialog(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_about:
                        Intent intent1 = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(intent1);
                        break;
                }
                return false;
            }
        });
        initialisePresenter();
        mPresenter.getTendersList(pageNo, "");
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNo = 1;
                totalPage = 1;
                mPresenter.getTendersList(pageNo, "refresh");            }
        });

    }

    public void initialisePresenter(){
        mPresenter = new TendersPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public void loadRecyclerViewTenders(TendersListAdapter adapter, int totalPages) {
        totalPage = totalPages;
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    Log.e("ErrorMsg", "cant scroll vertically");
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressLayout();
                        mPresenter.getTendersList(pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void showProgressbar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressbar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationProgressLayout() {
        paginationProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationProgressLayout() {
        paginationProgressLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void goToPdfViewActivity(String pdfUrl) {
        Intent intent = new Intent(this, ViewPdfActivity.class);
        intent.putExtra("PDF_URL", getResources().getString(R.string.base_url) + "/uploads/tender/" + pdfUrl);
        startActivity(intent);
    }

    @Override
    public void startDownload(String pdfUrl) {
        finalUrl = getResources().getString(R.string.base_url) + "/uploads/tender/" + pdfUrl;
        if(checkAndRequestPermissions()){
            new DownloadFileAsync().execute(finalUrl);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class DownloadFileAsync extends AsyncTask<String, String, String>{

        Boolean isError = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setTitle("Downloading file");
            progressDialog.setMax(100);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            int count;
            try{
                URL url = new URL(strings[0]);
                URLConnection urlConnection = url.openConnection();
                urlConnection.setRequestProperty("Accept-Encoding", "identity");
                urlConnection.connect();
                int length = urlConnection.getContentLength();
                InputStream inputStream = new BufferedInputStream(url.openStream());
                String fileName = UUID.randomUUID().toString();
                File file = new File(Environment.getExternalStorageDirectory(), "/Download/" + fileName + ".pdf");
                OutputStream outputStream = new FileOutputStream(file);
                byte data[] = new byte[1024];
                long total = 0;
                while((count = inputStream.read(data)) != -1){
                    total = total + count;
                    publishProgress("" + (int)((total*100)/length));
                    outputStream.write(data, 0, count);
                }
                outputStream.flush();
                outputStream.close();
                inputStream.close();
            }catch (Exception e){
                progressDialog.dismiss();
                isError = true;
                Log.e("ErrorMsg", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(isError){
                Toast.makeText(getApplicationContext(), "Download failed",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getApplicationContext(), "Download Success: Check inside download folder of your storage",Toast.LENGTH_SHORT).show();
            }

        }
    }

    public boolean checkAndRequestPermissions(){
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSIONS_REQUEST_CODE){
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i<grantResults.length; i++){
                if( grantResults[i] == PackageManager.PERMISSION_DENIED ){
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if(deniedCount == 0){
//               download file
                new DownloadFileAsync().execute(finalUrl);
            }

            else{
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()){
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, permName)){
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    }
                    else {
                        this.showAlertDialog("", "You have denied some permissions. Allow all permissions to download file at [Setting] > [Permissions]",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }
                }
            }

        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

}
