package in.net.webinfotech.dipr.presentation.presenters;

import in.net.webinfotech.dipr.domain.model.News;
import in.net.webinfotech.dipr.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.dipr.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.dipr.presentation.ui.adapters.NewsListAdapter;

/**
 * Created by Raj on 04-02-2019.
 */

public interface MainPresenter extends BasePresenter {
    void getNewsList(int pageNo, String type);
    interface View{
        void loadNewsRecyclerView(NewsListAdapter adapter, int totalPages);
        void loadNewsHeader(News news);
        void showProgressbar();
        void hideProgressbar();
        void showPaginationProgressLayout();
        void hidePaginationProgressLayout();
        void stopRefreshing();

    }
}
