package in.net.webinfotech.dipr.presentation.ui.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.dipr.R;

/**
 * Created by Raj on 25-01-2019.
 */

public class Fragment3 extends android.support.v4.app.Fragment {

    @BindView(R.id.recycler_view_main)
    RecyclerView recyclerView;
    @BindView(R.id.top_news_poster)
    ImageView imgTopPoster;
    LinearLayoutManager linearLayoutManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main,container,false);
        ButterKnife.bind(this, view);
        return view;
    }


}
