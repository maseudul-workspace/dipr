package in.net.webinfotech.dipr.presentation.presenters;

import in.net.webinfotech.dipr.domain.model.News;
import in.net.webinfotech.dipr.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 05-02-2019.
 */

public interface NewsDetailsPresenter extends BasePresenter {
    void getNewsDetails(int newsId);
    interface View{
        void loadNewsDetails(News news);
        void showProgressBar();
        void hideProgressBar();
    }
}
