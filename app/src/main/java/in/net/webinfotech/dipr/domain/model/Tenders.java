package in.net.webinfotech.dipr.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 05-02-2019.
 */

public class Tenders {
    @SerializedName("news_id")
    @Expose
    public int news_id;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("file")
    @Expose
    public String file;

    @SerializedName("description")
    @Expose
    public String description;
}
