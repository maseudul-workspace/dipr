package in.net.webinfotech.dipr.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.dipr.domain.executor.Executor;
import in.net.webinfotech.dipr.domain.executor.MainThread;
import in.net.webinfotech.dipr.domain.interactors.GetNewsDetailsInteractor;
import in.net.webinfotech.dipr.domain.interactors.impl.GetNewsDetailsInteractorImpl;
import in.net.webinfotech.dipr.domain.model.News;
import in.net.webinfotech.dipr.presentation.presenters.NewsDetailsPresenter;
import in.net.webinfotech.dipr.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.dipr.repository.News.NewsRepositoryImpl;

/**
 * Created by Raj on 05-02-2019.
 */

public class NewsDetailsPresenterImpl extends AbstractPresenter implements NewsDetailsPresenter, GetNewsDetailsInteractor.Callback {

    Context mContext;
    NewsDetailsPresenter.View mView;
    GetNewsDetailsInteractorImpl mInteractor;

    public NewsDetailsPresenterImpl(Executor executor,
                                    MainThread mainThread,
                                    Context context,
                                    NewsDetailsPresenter.View view) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;

    }

    @Override
    public void getNewsDetails(int newsId) {
        mInteractor = new GetNewsDetailsInteractorImpl(mExecutor, mMainThread, this, new NewsRepositoryImpl(), newsId);
        mInteractor.execute();
    }

    @Override
    public void onGettingNewsDetailsSuccess(News news) {
        mView.loadNewsDetails(news);
        mView.hideProgressBar();
    }

    @Override
    public void onGettingNewsDetailsFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
