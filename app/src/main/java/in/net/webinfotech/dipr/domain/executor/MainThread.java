package in.net.webinfotech.dipr.domain.executor;

/**
 * Created by Raj on 25-01-2019.
 */

public interface MainThread {
    /**
     * Make runnable operation run in the main thread.
     *
     * @param runnable The runnable to run.
     */
    void post(final Runnable runnable);
}
