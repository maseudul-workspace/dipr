package in.net.webinfotech.dipr.domain.interactors.impl;

import in.net.webinfotech.dipr.domain.executor.Executor;
import in.net.webinfotech.dipr.domain.executor.MainThread;
import in.net.webinfotech.dipr.domain.interactors.GetTendersListInteractor;
import in.net.webinfotech.dipr.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.dipr.domain.model.Tenders;
import in.net.webinfotech.dipr.domain.model.TendersWrapper;
import in.net.webinfotech.dipr.repository.Tenders.TendersRepositoryImpl;

/**
 * Created by Raj on 05-02-2019.
 */

public class GetTendersListInteractorImpl extends AbstractInteractor implements GetTendersListInteractor {

    Callback mCallback;
    int pageNo;
    TendersRepositoryImpl mRepository;

    public GetTendersListInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        TendersRepositoryImpl repository,
                                        int pageNo) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.pageNo = pageNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTendersListFail(errorMsg);
            }
        });
    }

    private void postMessage(final Tenders[] tenders, final int totalPages){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTendersListSuccess(tenders, totalPages);
            }
        });
    }


    @Override
    public void run() {
        final TendersWrapper tendersWrapper = mRepository.getTendersList(pageNo);
        if(tendersWrapper == null){
            notifyError("Something went wrong");
        }else if(!tendersWrapper.status){
            notifyError(tendersWrapper.message);
        }else{
            postMessage(tendersWrapper.tenders, tendersWrapper.total_page);
        }
    }
}
