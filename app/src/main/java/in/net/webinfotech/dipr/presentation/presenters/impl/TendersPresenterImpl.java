package in.net.webinfotech.dipr.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.dipr.domain.executor.Executor;
import in.net.webinfotech.dipr.domain.executor.MainThread;
import in.net.webinfotech.dipr.domain.interactors.GetTendersListInteractor;
import in.net.webinfotech.dipr.domain.interactors.impl.GetTendersListInteractorImpl;
import in.net.webinfotech.dipr.domain.model.Tenders;
import in.net.webinfotech.dipr.presentation.presenters.TendersPresenter;
import in.net.webinfotech.dipr.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.dipr.presentation.routers.TendersRouter;
import in.net.webinfotech.dipr.presentation.ui.adapters.TendersListAdapter;
import in.net.webinfotech.dipr.repository.Tenders.TendersRepositoryImpl;

/**
 * Created by Raj on 05-02-2019.
 */

public class TendersPresenterImpl extends AbstractPresenter implements TendersPresenter, GetTendersListInteractor.Callback,
                                                                                            TendersListAdapter.Callback{

    Context mContext;
    GetTendersListInteractorImpl mInteractor;
    TendersPresenter.View mView;
    TendersListAdapter adapter;
    TendersRouter mRouter;
    Tenders[] newTenders;

    public TendersPresenterImpl(Executor executor,
                                MainThread mainThread,
                                Context context,
                                TendersPresenter.View view,
                                TendersRouter router) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
        mRouter = router;
    }

    @Override
    public void getTendersList(int pageNo, String type) {
        if(type.equals("refresh")){
            newTenders = null;
        }
        mInteractor = new GetTendersListInteractorImpl(mExecutor, mMainThread, this, new TendersRepositoryImpl(), pageNo);
        mInteractor.execute();
    }

    @Override
    public void onGettingTendersListSuccess(Tenders[] tenders, int totalPages) {
        Tenders[] tempTenders;
        tempTenders = newTenders;
        try{
            int len1 = tempTenders.length;
            int len2 = tenders.length;
            newTenders = new Tenders[len1 + len2];
            System.arraycopy(tempTenders, 0, newTenders, 0, len1);
            System.arraycopy(tenders, 0, newTenders, len1, len2);
            adapter.addData(newTenders);
            adapter.notifyDataSetChanged();
            mView.hidePaginationProgressLayout();
        }catch (NullPointerException e){
            newTenders = tenders;
            adapter = new TendersListAdapter(mContext, tenders, this);
            mView.loadRecyclerViewTenders(adapter, totalPages);
            mView.hideProgressbar();
        }
        mView.stopRefreshing();
    }

    @Override
    public void onGettingTendersListFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideProgressbar();
        mView.hidePaginationProgressLayout();
        mView.stopRefreshing();
    }

    @Override
    public void onViewClicked(String pdfUrl) {
        mRouter.goToPdfViewActivity(pdfUrl);
    }

    @Override
    public void onDownloadClicked(String pdfUrl) {
        mView.startDownload(pdfUrl);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
