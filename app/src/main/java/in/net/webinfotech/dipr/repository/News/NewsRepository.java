package in.net.webinfotech.dipr.repository.News;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 04-02-2019.
 */

public interface NewsRepository {
    @POST("api/news_list.php")
    @FormUrlEncoded
    Call<ResponseBody> getNewsList(@Field("page_no") int page_no);

    @POST("api/news_details.php")
    @FormUrlEncoded
    Call<ResponseBody> getNewsDetails(@Field("news_id") int news_id);

}
